import React from 'react'
import Compare from './components/Compare'
import { BrowserRouter as Router, Route , Routes } from 'react-router-dom'
import Navbar from './components/Navbar';
import Spotlight from './components/pages/Spotlight';
import Categories from './components/pages/Categories';
import Product from './components/pages/Product';
import Faq from './components/pages/Faq';
import BlogSection from './components/pages/BlogSection';
import FooterSection from './components/pages/FooterSection';

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Compare />} />
      </Routes>
    </Router>
    // <>
    // //    <Navbar />
    // //    <Spotlight/>
    // //    <Categories/>
    // //   <Product/>
    // //   <Faq/>
    // //   <BlogSection/>
    // //   <FooterSection/>
    // // </>
  );
}

export default App
