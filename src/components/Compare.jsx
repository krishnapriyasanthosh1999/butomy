import React from 'react'
import Spotlight from './pages/Spotlight'
import Banking from './pages/Banking'
import Money from './pages/Money'
import Notification from './pages/Notification'
import Tools from './pages/Tools'
import Testimonial from './pages/Testimonial'
import Accounts from './pages/Accounts'
import Card from './pages/Card'
import BankingSection from './pages/BankingSection'
import BlogSection from './pages/BlogSection'
import FooterSection from './pages/FooterSection'
import Faq from './pages/Faq'
import Categories from './pages/Categories'
import Product from './pages/Product'

function Compare() {
  return (
    <div>
    <Spotlight/>
    <Categories/>
    <Product/>
    {/* <Banking/>
    <Money/>
    <Accounts/>
    <Notification/>
    <Tools/>
    <Card/>
    <Testimonial/>
    <BankingSection /> */}
    <Faq/>
    <BlogSection />
    <FooterSection />

    </div>
  )
}

export default Compare
