import React, { useState } from 'react';
import logo from '../assets/images/logo.svg';
import { Link } from 'react-router-dom';
import { HiMiniBars3, HiXMark } from 'react-icons/hi2'; 


function Navbar() {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  }

  return (
    // <section className=''>
      <div className="p-wrapper px-4 md:px-16 lg:px-32 py-4 md:py-2 lg:py-4  fixed left-0 right-0 z-10 bg-white">
        <div className="flex justify-between items-center ">
          <div className="w-24 md:w-36">
            {/* <img 
              src={logo} 
              alt="logo-image" 
              className="block w-full"
            /> */}
            <h1 className='text-[30px] font-[700] text-[#5BB5A2]'>Shopsy</h1>
          </div>
          <nav className="hidden md:flex gap-4 lg:gap-11 font-sans text-sm md:text-base font-medium">
            <Link to="/" className='p-[6px]'>Home</Link>
            <Link to="#" className=' p-[6px] '>About</Link>
            <Link to="#" className='p-[6px]'>Support</Link>
            <Link to="#" className=' p-[6px]'>Blog</Link>
          </nav>
          <div className=" hidden sm:hidden md:flex  lg:flex gap-4 lg:gap-8">
            <button className="text-[#5BB5A2] text-sm md:text-base font-medium">
                Login
            </button>
            <button className="bg-[#5BB5A2] py-2 px-3 md:px-4 rounded-md text-white text-[12px] md:text-[14px] font-sans">
                Open Account
            </button>
          </div>
          <div className="md:hidden flex items-center">
            <button className="text-[#5BB5A2] text-[25px] font-[700]" onClick={toggleMenu}>
              {menuOpen ? <HiXMark className='font-medium' /> : <HiMiniBars3 className='font-medium' />}
            </button>
          </div>
        </div>
        {menuOpen && (
          <nav className="flex flex-col gap-4 font-sans text-sm font-medium px-4 py-4 md:hidden">
            <Link to="#" onClick={() => setMenuOpen(false)}>Home </Link>
            <Link to="#" onClick={() => setMenuOpen(false)}>About</Link>
            <Link to="#" onClick={() => setMenuOpen(false)}>Support</Link>
            <Link to="#" onClick={() => setMenuOpen(false)}>Blog</Link>
            <div className="flex flex-col gap-4">
              <button className="text-[#5BB5A2] text-sm font-medium" onClick={() => setMenuOpen(false)}>
                Login
              </button>
              <button className="bg-[#5BB5A2] py-2 px-3 rounded-md text-white text-[12px] font-sans" onClick={() => setMenuOpen(false)}>
                Open Account
              </button>
            </div>
          </nav>
        )}
      </div>
    // </section>
  );
}

export default Navbar;
