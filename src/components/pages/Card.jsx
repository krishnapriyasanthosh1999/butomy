import React from "react";
import cardss from "../../assets/images/cardss (1).png";

function Card() {
  return (
    <div className="wrapper pt-16 sm:pt-32 px-4 sm:px-8 md:px-16 lg:px-56 items-center">
      <div className="flex flex-col items-center justify-center gap-8 sm:gap-11">
        <div className="flex flex-col items-center justify-center text-center">
          <span className="font-medium text-[16px] sm:text-[19px]">Account</span>
          <h1
            className="text-3xl sm:text-4xl font-medium lg:text-[68px]"
            style={{ lineHeight: "1.2", letterSpacing: "1.2px" }}
          >
            Perfect card <br />
            for your needs.
          </h1>
          <p
            className="mt-4 text-center text-sm sm:text-base lg:text-[1.2rem] font-medium"
            style={{ lineHeight: "32px" }}
          >
            Senectus et netus et malesuada fames ac turpis. <br />
            Sagittis vitae et leo duis ut diam.
          </p>
        </div>
        <div className="w-[300px] sm:w-[450px]">
          <img src={cardss} alt="" className="w-full block" />
        </div>
        <div className="pt-5 sm:pt-7 flex flex-col sm:flex-row gap-4 sm:gap-8">
          <button className="bg-[#5BB5A2] py-3 px-7 text-white rounded-md">
            Open Account
          </button>
          <button
            className="py-3 px-7 text-black rounded-md font-medium"
            style={{ border: "2px solid #E8E8E8" }}
          >
            Compare Cards
          </button>
        </div>
      </div>
    </div>
  );
}

export default Card;
