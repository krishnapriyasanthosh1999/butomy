import React from 'react';
import money from '../../assets/images/transactions.png';
import vect from '../../assets/icons/Vector (2).svg';

function Money() {
  return (
    <div className="wrapper pt-32 px-4 sm:px-8 md:px-20 lg:px-56 bg-[#E8F2EE] ">
      <div className="flex flex-col lg:flex-row gap-10 lg:gap-20">
        <div className="flex-1">
          <h1 className="text-4xl font-medium sm:text-5xl lg:text-[60px]" style={{ lineHeight: '1.2', letterSpacing: '1.3px' }}>
            Send & receive<br /> money instantly
          </h1>
          <p className="mt-4 text-base sm:text-lg lg:text-[1.2rem] font-medium text-[14px]">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br /> sed do eiusmod tempor incididunt ut labore et.
          </p>
          <div className="mt-6 space-y-4">
            <div className='flex items-center gap-4'>
              <img src={vect} alt="" className='bg-[#5BB5A2] p-1 h-[23px] w-[24px]  rounded-full' />
              <span className="text-[14px] lg:text-[16px] font-medium">Malesuada Ipsum</span>
            </div>
            <div className='flex items-center gap-4'>
              <img src={vect} alt="" className='bg-[#5BB5A2] p-1 h-[23px] w-[24px]   rounded-full' />
              <span className="text-[14px] lg:text-[16px] font-medium">Vestibulum</span>
            </div>
            <div className='flex items-center gap-4'>
              <img src={vect} alt="" className='bg-[#5BB5A2] p-1 h-[23px] w-[24px]   rounded-full' />
              <span className="text-[14px] lg:text-[16px] font-medium">Parturient Lorem</span>
            </div>
          </div>
        </div>
        <div className="flex-1 flex justify-center lg:justify-end">
          <img src={money} alt="Send & Receive Money" className="block w-full max-w-[375px]" />
        </div>
      </div>
    </div>
  );
}

export default Money;
