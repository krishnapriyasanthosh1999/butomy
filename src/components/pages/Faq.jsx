import React, { useState } from "react";
import { SlArrowUp, SlArrowDown } from "react-icons/sl";
import vect from '../../assets/icons/Vector.svg';
import mail from '../../assets/icons/mail.svg';
import ph from '../../assets/icons/ri_phone-fill.svg';
import { HiOutlinePlusSm } from "react-icons/hi";
import { HiMiniXMark } from "react-icons/hi2";

const Faq = () => {
  const [faqs, setFaqs] = useState([
    {
      question: "How do I open an Banko account?",
      answer:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dui accumsan sit amet nulla facilisi morbi. Eget gravida cum sociis natoque penatibus et magnis dis parturient.",
      isOpen: false,
    },
    {
      question: "How do I order a new card?",
      answer:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere consequatur, ipsam nemo aliquam deserunt totam cupiditate est neque qui saepe nostrum nisi, adipisci mollitia ea?",
      isOpen: false,
    },
    {
      question: "How to change my account limits?",
      answer:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere consequatur, ipsam nemo aliquam deserunt totam cupiditate est neque qui saepe nostrum nisi, adipisci mollitia ea?",
      isOpen: false,
    },
    {
      question: "How does Banko premium works?",
      answer:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius omnis ut necessitatibus expedita hic, consectetur neque error. Ipsa, eius aspernatur",
      isOpen: false,
    },
    {
      question: "Can I have two Banko accounts?",
      answer:
        "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Totam perspiciatis in est exercitationem pariatur odit veniam quibusdam. Quasi accusantium adipisci commodi unde temporibus.",
      isOpen: false,
    },
  ]);

  const toggleFaq = (index) => {
    const updatedFaqs = faqs.map((faq, i) =>
      i === index ? { ...faq, isOpen: !faq.isOpen } : faq
    );
    setFaqs(updatedFaqs);
  };

  return (
    <div className="p-wrapper px-4 md:px-20 lg:px-32 pb-16 md:py-32">
      <div className="flex flex-col md:flex-row justify-between">
        <div className="flex flex-col gap-5 md:w-1/3">
          <h1 className="text-black font-medium text-[40px] md:text-[60px] leading-normal ">
            Need Help?
          </h1>
          <div className="flex gap-4">
            <div className="w-[40px]">
              <img src={ph} alt="Phone" className="bg-[#E8F2EE] rounded-[20px] w-full p-[9px]" />
            </div>
            <h3 className="flex flex-col text-[16px] font-medium">
              +49 176 123 456
              <span className="text-[#1A191E80] text-[13px]">Support Hotline</span>
            </h3>
          </div>
          <div className="flex gap-4">
            <div className="w-[40px]">
              <img src={mail} alt="Email" className="bg-[#E8F2EE] rounded-[20px] w-full p-[10px]" />
            </div>
            <h3 className="flex flex-col text-[16px] font-medium">
              help@shop.com
              <span className="text-[#1A191E80] text-[13px]">Support Email</span>
            </h3>
          </div>
          <button className="text-[#5BB5A2] flex gap-3.5 items-center text-[15px] sm:text-[17px] font-medium mt-8 mb-8" >
            Support<img src={vect} alt="vector" />
          </button>
        </div>
        <div className="md:w-1/2">
          {faqs.map((faq, index) => (
            <div key={index} className="mb-6">
              <div className="flex justify-between items-center cursor-pointer" onClick={() => toggleFaq(index)}>
                <h1 className="text-[19px] font-[600]">
                  {faq.question}
                </h1>
                <button>
                  {faq.isOpen ? <HiMiniXMark className="text-[#5BB5A2] text-[26px]" /> : <HiOutlinePlusSm className="text-[#5BB5A2] text-[26px]" />}
                </button>
              </div>
              {faq.isOpen && <p className="mt-2 text-[15px] text-gray-500 font-[500]">{faq.answer}</p>}
              <div className="h-[1px] bg-[#D4D4D4] w-full mt-6"></div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Faq;
