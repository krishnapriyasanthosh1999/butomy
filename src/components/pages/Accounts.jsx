import React from "react";
import vectt from "../../assets/icons/Vector.svg";
import lap from "../../assets/icons/twemoji_laptop.svg";
import cycle from "../../assets/icons/twemoji_bicycle.svg";
import plane from "../../assets/icons/twemoji_airplane.svg";
import camera from "../../assets/icons/twemoji_camera-with-flash.svg";
import plus from "../../assets/icons/plus.svg";

function Accounts() {
  return (
    <div className="wrapper pt-32 px-4 sm:px-8 md:px-16 lg:px-56">
      <div className="flex flex-col gap-10">
        <div className="flex flex-col lg:flex-row justify-between main w-full">
          <div className="left">
            <span className="font-medium text-[16px] sm:text-[19px]">
              Saving Accounts
            </span>
            <h1
              className="text-3xl sm:text-4xl font-medium lg:text-[57px]"
              style={{ lineHeight: "1.2", letterSpacing: "1.3px" }}
            >
              Organize your
              <br /> money the right way
            </h1>
            <p
              className="mt-4 text-sm sm:text-base lg:text-[1.2rem] font-medium"
              style={{ lineHeight: "32px" }}
            >
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do{" "}
              <br className="hidden sm:block" />
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
          <div className="right mt-10 lg:mt-auto">
            <button
              className="text-[#5BB5A2] flex gap-3.5 items-center text-[16px] sm:text-[17px] font-medium"
              style={{ marginTop: "55px" }}
            >
              Compare Cards{" "}
              <img src={vectt} alt="vector" className="w-[16px]" />
            </button>
          </div>
        </div>
        <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-5 gap-4">
          <div className="flex flex-col items-center">
            <div className="bg-[#E8F2EE] p-8 sm:p-[77px] rounded-[20px] text-white w-[150px] sm:w-[195px] flex flex-col items-center">
              <img src={lap} alt="" />
            </div>
            <h6 className="text-[16px] font-[600] mt-2">New Laptop</h6>
            <p className="text-sm text-left font-[500] text-[#1A191E80]">
              4005$
            </p>
          </div>
          <div className="flex flex-col items-center">
            <div className="bg-[#F1DFDF] p-8 sm:p-[77px] rounded-[20px] text-white w-[150px] sm:w-[195px] flex flex-col items-center">
              <img src={cycle} alt="" />
            </div>
            <h6 className="text-[16px] font-[600] mt-2">Dream Bike</h6>
            <p className="text-sm text-left font-[500] text-[#1A191E80]">
              4005$
            </p>
          </div>
          <div className="flex flex-col items-center">
            <div className="bg-[#DFE1F1] p-8 sm:p-[77px] rounded-[20px] text-white w-[150px] sm:w-[195px] flex flex-col items-center">
              <img src={plane} alt="" />
            </div>
            <h6 className="text-[16px] font-[600] mt-2">Holiday</h6>
            <p className="text-sm text-left font-[500] text-[#1A191E80]">
              4005$
            </p>
          </div>
          <div className="flex flex-col items-center">
            <div className="bg-[#DFEBF1] p-8 sm:p-[77px] rounded-[20px] text-white w-[150px] sm:w-[195px] flex flex-col items-center">
              <img src={camera} alt="" />
            </div>
            <h6 className="text-[16px] font-[600] mt-2">Camera</h6>
            <p className="text-sm text-left font-[500] text-[#1A191E80]">
              4005$
            </p>
          </div>
          <div className="flex flex-col items-center">
            <div className="bg-[#F8F8F8] p-8 sm:p-[80px] rounded-[20px] text-white w-[150px] sm:w-[195px] flex flex-col items-center">
              <img src={plus} alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Accounts;
