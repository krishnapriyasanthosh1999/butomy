import React from 'react';
import atm from '../../assets/images/atm.png';
import phone from '../../assets/images/phone.png';
import cash from '../../assets/images/cash.png';
import vectt from "../../assets/icons/Vector.svg";
import shop from '../../assets/images/shop.jpg'

const BlogSection = () => {
  const blogs = [
    {
      image: shop,
      title: 'Current shoe obessions',
      description: 'A shopping cart, trolley, or buggy, also known by a variety of other names, is a wheeled cart supplied by a shop or store, especially supermarkets, for use by customers inside the premises for transport.',
    },
    {
      image: phone,
      title: 'Current shoe obessions',
      description: 'A shopping cart, trolley, or buggy, also known by a variety of other names, is a wheeled cart supplied by a shop or store, especially supermarkets, for use by customers inside the premises for transport.',
    },
    {
      image: cash,
      title: 'Current shoe obessions',
      description: 'A shopping cart, trolley, or buggy, also known by a variety of other names, is a wheeled cart supplied by a shop or store, especially supermarkets, for use by customers inside the premises for transport.',
    },
  ];

  return (
    <div className="p-wrapper pb-16 md:pb-32 px-4 md:px-20 lg:px-32">
      <div className="flex flex-col md:flex-row justify-between items-center mb-8">
        <h2 className="font-dm-sans font-medium text-[36px] md:text-[48px] lg:text-[58px] leading-tight tracking-[-0.03em]">Blog</h2>
        <div className="right mt-6 md:mt-0">
          <button className="text-[#5BB5A2] flex gap-3.5 items-center text-[16px] sm:text-[17px] font-medium">
            All Articles
            <img src={vectt} alt="vector" className="w-[16px]" />
          </button>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
        {blogs.map((blog, index) => (
          <div key={index} className="bg-white rounded-[16px] shadow-md overflow-hidden">
            <img
              src={blog.image}
              alt={`Blog ${index + 1}`}
              className="w-full h-64 object-cover rounded-t-[16px]"
            />
            <div className="p-6">
              <h3 className="font-dm-sans font-medium text-[20px] md:text-[23px] leading-tight tracking-[-0.03em] mb-2">{blog.title}</h3>
              <p className="font-dm-sans font-medium text-[14px] md:text-[15px] leading-relaxed tracking-[-0.03em] mb-4">{blog.description}</p>
              <div className="flex flex-wrap gap-2">
                <button className="px-4 py-1 rounded bg-[#F8F8F8] text-black font-[500] text-[12px]">Product</button>
                
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default BlogSection;
