import React from "react";
import starr from '../../assets/icons/star.svg'
import rating from  '../../assets/icons/rating.svg'

function Testimonial() {
  return (
    <div className="wrapper py-16 md:py-32 px-4 md:px-20 lg:px-56">
      <div>
        <div className="flex flex-col md:flex-row justify-between items-start md:items-center mb-8">
          <div className="mb-4 md:mb-0">
            <span className="font-medium text-[16px] sm:text-[19px]">
              Testimonials
            </span>
            <h1 className="text-3xl sm:text-4xl font-medium lg:text-[48px] xl:text-[60px]" style={{ lineHeight: "1.2", letterSpacing: "1.3px" }}>
              People all over the <br className="hidden md:block"/> world use Banko.
            </h1>
          </div>
          <div className="flex gap-2 items-end">
            <div className="">
              <img src={starr} alt="star image" className="bg-[#E8F2EE] p-1 h-[21px] w-[22px] rounded-full" />
            </div>
            <div className="">
              <p className="text-[16px] sm:text-[18px] font-medium">Rated <span className="text-[#5BB5A2]">4.8/5</span> from over 1000 users</p>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
          {Array(6).fill(0).map((_, index) => (
            <div key={index} className="border p-[20px] rounded-[16px] flex flex-col gap-2">
              <div className="w-[80px] sm:w-[100px]">
                <img src={rating} alt="rating image" className="w-full" />
              </div>
              <h1 className="text-[20px] sm:text-[25px] font-[600]">
                {index % 2 === 0 ? "Sunt qui esse pariatur duis deserunt mollit" : "Elit aute irure tempor cupidatat incididunt"}
              </h1>
              <p className="text-[14px] sm:text-[16px] font-medium text-[#1A191E]">
                Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. Sunt qui esse pariatur duis deserunt mollit dolore
                cillum minim tempor enim. Elit aute irure tempor cupidatat incididunt sint deserunt ut voluptate aute id deserunt nisi.
              </p>
              <h6 className="flex flex-col gap-1 font-medium text-[15px] sm:text-[17px]">
                {index % 2 === 0 ? "Cody Fisher" : "Guy Hawkins"} 
                <span className="text-[#1A191E80] text-[14px]">
                  {index % 2 === 0 ? "Medical Assistant" : "President of Sales"}
                </span>
              </h6>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Testimonial;
