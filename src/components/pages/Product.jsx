import React, { useState } from 'react';
import { FaStar } from 'react-icons/fa';
import phone from '../../assets/images/phone.jpg';
import lap from '../../assets/images/lap.jpg';
import t1 from '../../assets/images/t1.jpg';
import t2 from '../../assets/images/t2.jpg';
import t3 from '../../assets/images/t3.jpg';
import t4 from '../../assets/images/t4.jpg';
import p2 from '../../assets/images/p2.jpg';
import p3 from '../../assets/images/p3.jpg';

const productss = [
  {
    name: 'Electronics',
    products: [
      { name: 'Smartphone', image: phone, price: 699, rating: 4 },
      { name: 'Laptop', image: lap, price: 999, rating: 5 },
      { name: 'Smartphone', image: p2, price: 699, rating: 4 },
      { name: 'Laptop', image: p3, price: 999, rating: 5 },
    ],
  },
  {
    name: 'Fashion',
    products: [
      { name: 'Men', image: t1, price: 29, rating: 4 },
      { name: 'Bag', image: t2, price: 59, rating: 4 },
      { name: 'Women', image: t3, price: 89, rating: 5 },
      { name: 'Jacket', image: t4, price: 129, rating: 5 },
    ],
  },

];

function Product() {
  return (
    <div className="p-wrapper  px-4 md:px-16 lg:px-32 py-8">
      <h1 className=" font-bold text-[25px] mb-4">SHOP NOW</h1>
      {productss.map((category) => (
        <div key={category.name} className="mb-8">
          {/* <h2 className="text-xl md:text-2xl font-bold mb-4">{category.name}</h2> */}
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4">
            {category.products.map((product, index) => (
              <ProductCard key={index} product={product} />
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}

function ProductCard({ product }) {
  const [quantity, setQuantity] = useState(1);

  const increaseQuantity = () => {
    setQuantity(quantity + 1);
  };

  const decreaseQuantity = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const totalPrice = product.price * quantity;

  return (
    <div className="border p-4 rounded-lg">
      <img src={product.image} alt={product.name} className="w-full h-32 object-cover mb-2" />
      <h3 className="text-lg font-medium">{product.name}</h3>
      <div className="flex items-center">
        {[...Array(5)].map((star, i) => (
          <FaStar key={i} color={i < product.rating ? "#FFD700" : "#E4E5E9"} />
        ))}
      </div>
      <p className="text-gray-700">${product.price.toFixed(2)}</p>
      <div className="flex items-center mt-2">
        <button onClick={decreaseQuantity} className="px-2 py-1 bg-gray-300 rounded">-</button>
        <span className="px-4">{quantity}</span>
        <button onClick={increaseQuantity} className="px-2 py-1 bg-gray-300 rounded">+</button>
      </div>
      <p className="text-gray-700 mt-2">Total: ${totalPrice.toFixed(2)}</p>
    </div>
  );
}

export default Product;
