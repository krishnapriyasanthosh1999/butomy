import React from 'react';
import shop2 from '../../assets/images/shop2.jpg';
import vect from '../../assets/icons/Vector.svg';

function Spotlight() {
  return (
    <div className="relative w-full  bg-cover bg-center bg-black/10" style={{ 
      backgroundImage: `url(${shop2})`,
    }}>
      
      <div 
        className='p-8 sm:p-16 lg:p-24 flex flex-col gap-8 sm:gap-12 md:gap-24 relative ' 
       
      >
        <div className='flex flex-col justify-center items-center text-center px-4 sm:px-0 mt-[100px] '>
          <span className='text-[16px] sm:text-[19px] font-medium'> Welcome</span>
          <h1 className='text-[36px] sm:text-[56px] lg:text-[71px] font-semibold mt-4 md:mt-6'>
            Fashion World.
          </h1>
          <p className='text-[16px] sm:text-[19px] font-medium leading-6 sm:leading-8 mt-4'>
            Fashion is best defined simply as the style or styles of clothing and <br/>
            accessories worn at any given time by groups of people.
          </p>
          <div className='mt-6 sm:mt-8 flex flex-col sm:flex-row gap-4 sm:gap-8 items-center'>
            <button className='bg-[#5BB5A2] py-3 px-7 text-white rounded-md'>
              Open Account
            </button>
            <button className='text-[#5BB5A2] flex items-center text-[16px] sm:text-[17px] font-medium'>
              Explore All <img src={vect} alt="vector" className='ml-2' />
            </button>
          </div>
        </div>
        {/* Optional: Uncomment if you want to include an image */}
        {/* <div className='hidden md:block md:w-1/2'>
          <img
            src={cards}
            alt="spotlight-image"
            className='block w-full'
          />
        </div> */}
      </div>
    </div>
  );
}

export default Spotlight;
