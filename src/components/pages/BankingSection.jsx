import React from "react";
import vect from "../../assets/icons/white-ti.svg";
import app from '../../assets/images/app-1.png';
import apple from "../../assets/icons/Apple Store.svg";
import store from "../../assets/icons/Google Store.svg";

const BankingSection = () => {
  return (
    <div className="wrapper pb-16 md:pb-32 px-4 md:px-20 lg:px-52">
      <div className="bg-[#5BB5A2] flex flex-col md:flex-col lg:flex-row p-6 md:p-8 rounded-[20px] text-white justify-between">
        <div className="md:w-1/2 p-4">
          <h1 className="text-[40px] md:text-[60px] mb-4 font-[500] whitespace-nowrap" style={{ lineHeight: "normal" }}>
            One app. <br />
            One banking.
          </h1>
          <p className="mb-4 text-[16px] md:text-[18px] md:w-[145%]">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.
          </p>
          <div className="mt-6 space-y-4">
            <div className="flex flex-col md:flex-row gap-6">
              <div className="flex flex-col gap-2">
                <div className="flex items-center gap-2 whitespace-nowrap">
                  <img src={vect} alt="" className="bg-[#FFFFFF33] p-1 h-[27px] w-[28px] rounded-full" />
                  <span className="text-[14px] lg:text-[16px] font-medium">Instant transactions</span>
                </div>
                <div className="flex items-center gap-2 whitespace-nowrap">
                  <img src={vect} alt="" className="bg-[#FFFFFF33] p-1 h-[27px] w-[28px] rounded-full" />
                  <span className="text-[14px] lg:text-[16px] font-medium">Payments worldwide</span>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div className="flex items-center gap-2 whitespace-nowrap">
                  <img src={vect} alt="" className="bg-[#FFFFFF33] p-1 h-[27px] w-[28px] rounded-full" />
                  <span className="text-[14px] lg:text-[16px] font-medium">Saving accounts</span>
                </div>
                <div className="flex items-center gap-2 whitespace-nowrap">
                  <img src={vect} alt="" className="bg-[#FFFFFF33] p-1 h-[27px] w-[28px] rounded-full" />
                  <span className="text-[14px] lg:text-[16px] font-medium">100% mobile banking</span>
                </div>
              </div>
            </div>
            <div className="w-[130px]">
              <img src={store} alt="Google Store" className="w-full" />
            </div>
          </div>
        </div>
        <div className="p-4 flex items-center justify-center md:relative md:top-12 mt-8 md:mt-0">
          <img src={app} alt="Main Image" className="w-full block" />
        </div>
      </div>
    </div>
  );
};

export default BankingSection;
