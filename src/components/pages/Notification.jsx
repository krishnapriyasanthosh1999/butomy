import React from "react";
import money from '../../assets/images/transactions.png';
import vect from '../../assets/icons/green-fill.svg'
import vectt from '../../assets/icons/Vector.svg'
import notifi from '../../assets/images/notifications.png'

function Notification() {
  return (
    <div className="wrapper py-32 px-4 sm:px-8 md:px-20 lg:px-56">
      <div className="flex flex-col lg:flex-row gap-10 lg:gap-20">
        <div className="flex-1">
          <span className="font-medium text-[16px] sm:text-[19px]">Notifications</span>
          <h1
            className="text-3xl sm:text-4xl font-medium lg:text-[60px]"
            style={{ lineHeight: "1.2", letterSpacing: "1.3px" }}
          >
            Stay notified
          </h1>
          <p className="mt-4 text-sm sm:text-base lg:text-[1.1rem] font-medium">
            Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia
            consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
          </p>
          <div className="mt-6 space-y-4">
            <div className="flex items-center gap-4">
              <img
                src={vect}
                alt=""
                className="bg-[#E8F2EE] p-1 h-[27px] w-[28px] rounded-full"
              />
              <span className="text-[14px] lg:text-[16px] font-medium">
                Malesuada Ipsum
              </span>
            </div>
            <div className="flex items-center gap-4">
              <img
                src={vect}
                alt=""
                className="bg-[#E8F2EE] p-1 h-[27px] w-[28px] rounded-full"
              />
              <span className="text-[14px] lg:text-[16px] font-medium">
                Vestibulum
              </span>
            </div>
            <div className="flex items-center gap-4">
              <img
                src={vect}
                alt=""
                className="bg-[#E8F2EE] p-1 h-[27px] w-[28px] rounded-full"
              />
              <span className="text-[14px] lg:text-[16px] font-medium">
                Parturient Lorem
              </span>
            </div>
            <button className='text-[#5BB5A2] flex gap-3.5 items-center text-[16px] sm:text-[17px] font-medium mt-10' style={{marginTop:'55px'}}>
              Compare Cards <img src={vectt} alt="vector" className="w-[16px]" />
            </button>
          </div>
        </div>
        <div className="flex-1 flex justify-center lg:justify-end">
          <img
            src={notifi}
            alt="Send & Receive Money"
            className="block w-full max-w-[375px]"
          />
        </div>
      </div>
    </div>
  );
}

export default Notification;
