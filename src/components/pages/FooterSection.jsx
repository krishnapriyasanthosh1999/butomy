import React from 'react';
import logo from '../../assets/images/logo.svg';

const FooterSection = () => {
  return (
    <footer className="p-wrapper px-6 md:px-20 lg:px-32 pt-8 md:pt-16 pb-8" style={{borderTop:"1px solid #E8E8E8"}}>
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-[7rem] p-[24px]" style={{borderBottom:"1px solid #E8E8E8"}}>
        <div className="col-span-2 sm:col-span-1 md:col-span-1">
          <div className="w-24 md:w-36">
            {/* <img 
              src={logo} 
              alt="logo-image" 
              className="block w-full" 
            /> */}
            <h1 className='text-[30px] font-[700] text-[#5BB5A2]'>Shopsy</h1>
          </div>
        </div>
        <div>
          <h3 className="font-dm-sans text-lg font-semibold mb-4 text-black">About</h3>
          <ul className='text-[#1A191E80] font-medium'>
            <li className="mb-2">Features</li>
            <li className="mb-2">Pricing</li>
            <li className="mb-2">Support</li>
          </ul>
        </div>
        <div>
          <h3 className="font-dm-sans text-lg font-semibold mb-4 text-black">Blog</h3>
          <ul className='text-[#1A191E80] font-medium'>
            <li className="mb-2">Products</li>
            <li className="mb-2">Technology</li>
            <li className="mb-2">Crypto</li>
          </ul>
        </div>
        <div>
          <h3 className="font-dm-sans text-lg font-semibold mb-4 text-black">Webflow</h3>
          <ul className='text-[#1A191E80] font-medium'>
            <li className="mb-2">Styleguide</li>
            <li className="mb-2">Licensing</li>
            <li className="mb-2">Changelog</li>
          </ul>
        </div>
        <div>
          <h3 className="font-dm-sans text-lg font-semibold mb-4 text-black">Changelog</h3>
          <ul className='text-[#1A191E80] font-medium'>
            <li className="mb-2">Twitter</li>
            <li className="mb-2">Facebook</li>
            <li className="mb-2">Instagram</li>
          </ul>
        </div>
      </div>
      <div className='flex flex-col md:flex-row justify-between py-5 font-medium text-[14px] text-[#1A191E80]'>
        <p className="mb-4 md:mb-0">©  Made by <span className='text-[#5BB5A2]'>shop now</span>  - Powered by <span className='text-[#5BB5A2]'>Webflow</span></p>
        <div className='flex flex-col sm:flex-row gap-2'>
          <p>Impressum</p>
          <p>Datenschutz</p>
        </div>
      </div>
    </footer>
  );
};

export default FooterSection;
