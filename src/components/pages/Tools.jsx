import React from "react";
import webflow from "../../assets/icons/Webflow_logo 1.svg";
import shopify from "../../assets/icons/Shopify_logo_2018 1.svg";
import zapier from "../../assets/icons/Zapier_logo 1.svg";
import bitcoin from "../../assets/icons/Group.svg";
import paypal from "../../assets/icons/PayPal 1.svg";
import circle from "../../assets/icons/Mastercard-logo 1.svg";
import visa from "../../assets/icons/Visa_2021 1.svg";
import gpay from "../../assets/icons/Google_Pay_(GPay)_Logo_(2018-2020) 1.svg";
import apple from "../../assets/icons/Apple_Pay_logo 1.svg";
import amazone from "../../assets/icons/Amazon_Pay_logo 1.svg";
import vect from "../../assets/icons/green-fill.svg";

function Tools() {
  return (
    <div className="wrapper pb-32 px-4 sm:px-8 md:px-16 lg:px-56 " style={{borderBottom:'1px solid #E8E8E8'}}>
      <div>
        <div className="flex flex-col gap-6 items-baseline">
          <div className="flex flex-wrap gap-6 items-center justify-center">
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[90px] sm:w-[117px]">
              <img src={webflow} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[90px] sm:w-[117px]">
              <img src={shopify} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[70px] sm:w-[90px]">
              <img src={zapier} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[90px] sm:w-[117px]">
              <img src={bitcoin} alt="" className="w-full block" />
            </div>
          </div>
          <div className="flex flex-wrap gap-6 items-center justify-center">
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[90px] sm:w-[117px]">
              <img src={paypal} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[70px] sm:w-[90px]">
              <img src={circle} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[70px] sm:w-[87px]">
              <img src={visa} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[70px] sm:w-[87px]">
              <img src={gpay} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[70px] sm:w-[87px]">
              <img src={apple} alt="" className="w-full block" />
            </div>
            <div className="bg-[#F8F8F8] p-4 rounded-[13px] w-[90px] sm:w-[120px]">
              <img src={amazone} alt="" className="w-full block" />
            </div>
          </div>
          <div className="flex flex-col gap-6 mt-6 lg:flex-row lg:gap-32">
            <div>
              <span className="font-medium text-[19px]">Tools</span>
              <h1
                className="text-4xl font-medium sm:text-5xl lg:text-[55px]"
                style={{ lineHeight: "1.2", letterSpacing: "1.3px" }}
              >
                Seemless <br />
                integration
              </h1>
              <p className="mt-4 text-base sm:text-lg lg:text-[1.1rem] font-medium text-[14px]">
                Amet minim mollit non deserunt ullamco est sit aliqua dolor do{" "}
                <br className="hidden sm:block" /> amet sint. Velit officia
                consequat duis enim velit mollit.
              </p>
            </div>

            <div className="mt-8 lg:mt-32 space-y-4 flex flex-col justify-center">
              <div className="flex items-center gap-4">
                <img
                  src={vect}
                  alt=""
                  className="bg-[#E8F2EE] p-1 h-[35px] w-[36px] rounded-full"
                />
                <span className="text-[14px] lg:text-[16px] font-medium">
                  Secure and encrypted integration
                </span>
              </div>
              <div className="flex items-center gap-4">
                <img
                  src={vect}
                  alt=""
                  className="bg-[#E8F2EE] p-1 h-[35px] w-[36px] rounded-full"
                />
                <span className="text-[14px] lg:text-[16px] font-medium">
                  Fully API interface
                </span>
              </div>
              <div className="flex items-center gap-4">
                <img
                  src={vect}
                  alt=""
                  className="bg-[#E8F2EE] p-1 h-[35px] w-[36px] rounded-full"
                />
                <span className="text-[14px] lg:text-[16px] font-medium">
                  Payments worldwide
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Tools;
