import React from 'react'
import instant from '../../assets/icons/Instant.svg'
import acc from '../../assets/icons/accounts.svg'
import app from '../../assets/images/app.jpg'
import mob from '../../assets/icons/mobile.svg'
import stat from '../../assets/icons/stati.svg'
import vir from '../../assets/icons/virtualcard.svg'
import wi from '../../assets/icons/wifi.svg'

function Banking() {
  return (
    <div>
      <div className="wrapper">
      <div class='py-32 px-4 sm:px-16 md:px-32 lg:px-40 flex flex-col lg:flex-row gap-10 lg:gap-20 items-center lg:items-center'>
          <div className='font-sans flex flex-col gap-16'>
            <h1 className='text-4xl sm:text-5xl lg:text-[70px] font-semibold leading-[1.2]'>
              One app. <br/> One banking.
            </h1>
            <div className='grid  grid-cols-1 sm:grid-cols-2 items-center md:grid-cols-2 gap-10 lg:gap-20'>
              <div className='border-2 rounded-2xl p-6  w-[250px]'>
                <div className='bg-[#E8F2EE] p-4 w-[50px] h-[50px] rounded-full'>
                  <img src={instant} alt="instant vector" className='block w-full' />
                </div>
                <div>
                  <h3 className='text-[19px] font-medium my-2'>Instant <br/>transactions</h3>
                  <p className='text-[15px] font-medium'>Odio euismod lacinia at quis. Amet purus gravida quis blandit turpis.</p>
                </div>
              </div>
              <div className='border-2 rounded-2xl p-6  w-[250px]'>
                <div className='bg-[#E8F2EE] p-4 w-[50px] h-[50px] rounded-full'>
                  <img src={acc} alt="accounts vector" className='block w-full' />
                </div>
                <div>
                  <h3 className='text-[19px] font-medium my-2'>Instant <br/>transactions</h3>
                  <p className='text-[15px] font-medium'>Odio euismod lacinia at quis. Amet purus gravida quis blandit turpis.</p>
                </div>
              </div>
              <div className='border-2 rounded-2xl p-6 w-[250px]'>
                <div className='bg-[#E8F2EE] p-4 w-[50px] h-[50px] rounded-full'>
                  <img src={mob} alt="mobile vector" className='block w-full' />
                </div>
                <div>
                  <h3 className='text-[19px] font-medium my-2'>Instant <br/>transactions</h3>
                  <p className='text-[15px] font-medium'>Odio euismod lacinia at quis. Amet purus gravida quis blandit turpis.</p>
                </div>
              </div>
              <div className='border-2 rounded-2xl p-6 w-[250px]'>
                <div className='bg-[#E8F2EE] p-4 w-[50px] h-[50px] rounded-full'>
                  <img src={stat} alt="statistics vector" className='block w-full' />
                </div>
                <div>
                  <h3 className='text-[19px] font-medium my-2'>Instant <br/>transactions</h3>
                  <p className='text-[15px] font-medium'>Odio euismod lacinia at quis. Amet purus gravida quis blandit turpis.</p>
                </div>
              </div>
              <div className='border-2 rounded-2xl p-6 w-[250px]'>
                <div className='bg-[#E8F2EE] p-4 w-[50px] h-[50px] rounded-full'>
                  <img src={vir} alt="virtual card vector" className='block w-full' />
                </div>
                <div>
                  <h3 className='text-[19px] font-medium my-2'>Instant <br/>transactions</h3>
                  <p className='text-[15px] font-medium'>Odio euismod lacinia at quis. Amet purus gravida quis blandit turpis.</p>
                </div>
              </div>
              <div className='border-2 rounded-2xl p-6 w-[250px]'>
                <div className='bg-[#E8F2EE] p-4 w-[50px] h-[50px] rounded-full'>
                  <img src={wi} alt="wifi vector" className='block w-full' />
                </div>
                <div>
                  <h3 className='text-[19px] font-medium my-2'>Instant <br/>transactions</h3>
                  <p className='text-[15px] font-medium'>Odio euismod lacinia at quis. Amet purus gravida quis blandit turpis.</p>
                </div>
              </div>
            </div>
          </div>
          <div className=' lg:justify-end'>
            <img src={app} alt="app image" className='block w-full max-w-[375px]' />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Banking
