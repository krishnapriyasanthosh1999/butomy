import React from 'react'
import phone from '../../assets/images/phone.jpg'
import lap from '../../assets/images/lap.jpg'
import t1 from '../../assets/images/t1.jpg'
import t2 from '../../assets/images/t2.jpg'
import t3 from '../../assets/images/t3.jpg'
import t4 from '../../assets/images/t4.jpg'
import p2 from '../../assets/images/p2.jpg'
import p3 from '../../assets/images/p3.jpg'


const categories = [
  {
    name: 'Electronics',
    products: [
      { name: 'Smartphone', image: phone },
      { name: 'Laptop', image: lap },
      { name: 'Smartphone', image: p2 },
      { name: 'Laptop', image: p3 },
    ],
  },
  {
    name: 'Fashion',
    products: [
      { name: 'Men', image: t1 },
      { name: 'Bag', image: t2 },
      { name: 'Women', image: t3 },
      { name: 'Jacket', image: t4 },
    ],
  },
  {
    name: 'Electronics',
    products: [
      { name: 'Smartphone', image: phone },
      { name: 'Laptop', image: lap },
      { name: 'Smartphone', image: p2 },
      { name: 'Laptop', image: p3 },
    ],
  },
  {
    name: 'Fashion',
    products: [
      { name: 'Men', image: t1 },
      { name: 'Bag', image: t2 },
      { name: 'Women', image: t3 },
      { name: 'Jacket', image: t4 },
    ],
  },
];


function Categories() {
    return (
        <div className="p-wrapper  px-4 md:px-16 lg:px-32 py-8">
            <h1 className=' font-bold text-[25px] mb-4'>ALL CATERGORIES</h1>
          {categories.map((category) => (
            <div key={category.name} className="mb-8">
              <h2 className="text-[18px] md:text-[20px]  mb-4">{category.name}</h2>
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-4">
                {category.products.map((product) => (
                  <div key={product.name} className="border rounded-lg overflow-hidden">
                    <img src={product.image} alt={product.name} className="w-full h-32 object-cover mb-2 " />
                    <h3 className="text-[16px] font-medium p-3 text-gray-400">{product.name}</h3>
                  </div>
                ))}
              </div>
            </div>
          ))}
        </div>
      );
}

export default Categories
